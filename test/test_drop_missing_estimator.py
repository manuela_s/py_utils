#!/usr/bin/env python3

import os
import unittest
import unittest.mock

import pandas
import pandas.testing
import sklearn.base
import sklearn.linear_model

import py_utils.drop_missing_estimator


class DropMissingEstimatorTest(unittest.TestCase):
    def setUp(self):
        self.data = pandas.read_csv(os.path.join('lung.csv'))
        self.base_estimator = unittest.mock.MagicMock(spec=sklearn.linear_model.LinearRegression)
        self.estimator = py_utils.drop_missing_estimator.DropMissingEstimator(self.base_estimator)

    def testDropsNaX(self):
        """Test that the estimator drops rows where x has NaNs"""
        self.estimator.fit(self.data.drop(columns=['time']), self.data[['time']])
        pandas.testing.assert_frame_equal(self.data.dropna().drop(columns=['time']), self.base_estimator.fit.call_args[0][0])
        pandas.testing.assert_frame_equal(self.data.dropna()[['time']], self.base_estimator.fit.call_args[0][1])

    def testDropsNaY(self):
        """Test that the estimator drops rows where y has NaNs"""
        # Add a Nan to the 'time' column used as y for fit.
        # Use row 1, because it does not have NaNs in any other column
        self.data.loc[1, 'time'] = None
        self.estimator.fit(self.data.drop(columns=['time']), self.data[['time']])
        pandas.testing.assert_frame_equal(self.data.dropna().drop(columns=['time']), self.base_estimator.fit.call_args[0][0])
        pandas.testing.assert_frame_equal(self.data.dropna()[['time']], self.base_estimator.fit.call_args[0][1])

    def testSeries(self):
        """Test that the estimator works even if y is a series."""
        self.estimator.fit(self.data.drop(columns=['time']), self.data.time)
        pandas.testing.assert_frame_equal(self.data.dropna().drop(columns=['time']), self.base_estimator.fit.call_args[0][0])
        pandas.testing.assert_series_equal(self.data.dropna().time, self.base_estimator.fit.call_args[0][1])


if __name__ == '__main__':
    unittest.main()
