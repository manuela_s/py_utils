#!/usr/bin/env python3

import unittest

import pandas
import rpy2
import rpy2.robjects
import rpy2.robjects.packages
import rpy2.robjects.pandas2ri

import py_utils.maaslin_utils


class MaaslinTestCase(unittest.TestCase):
    @staticmethod
    def get_input_data():
        base = rpy2.robjects.packages.importr("base")
        with (
            rpy2.robjects.default_converter + rpy2.robjects.pandas2ri.converter
        ).context():
            (input_data,) = base.system_file(
                "extdata", "HMP2_taxonomy.tsv", package="Maaslin2", mustWork=True
            )
        return pandas.read_csv(input_data, sep="\t", index_col=0)

    @staticmethod
    def get_input_metadata():
        base = rpy2.robjects.packages.importr("base")
        with (
            rpy2.robjects.default_converter + rpy2.robjects.pandas2ri.converter
        ).context():
            (input_metadata,) = base.system_file(
                "extdata", "HMP2_metadata.tsv", package="Maaslin2", mustWork=True
            )
        return pandas.read_csv(input_metadata, sep="\t", index_col=0)

    def test_python_wrapper_results_match_demo_r_defaults(self):
        results = py_utils.maaslin_utils.Maaslin(
            input_data=MaaslinTestCase.get_input_data(),
            input_metadata=MaaslinTestCase.get_input_metadata(),
            normalization="TSS",
            transform="LOG",
            analysis_method="LM",
            random_effects=["site", "subject"],
            fixed_effects=[
                "diagnosis",
                "dysbiosisnonIBD",
                "dysbiosisUC",
                "dysbiosisCD",
                "antibiotics",
                "age",
            ],
            reference={"diagnosis": "nonIBD"},
        )
        self.assertEqual(results.results.query("qval.lt(0.25)").shape[0], 241)

    def test_python_wrapper_results_match_demo_r_results(self):
        results = py_utils.maaslin_utils.Maaslin(
            input_data=MaaslinTestCase.get_input_data(),
            input_metadata=MaaslinTestCase.get_input_metadata(),
            normalization="NONE",
            transform="AST",
            analysis_method="LM",
            random_effects=["site", "subject"],
            fixed_effects=[
                "diagnosis",
                "dysbiosisnonIBD",
                "dysbiosisUC",
                "dysbiosisCD",
                "antibiotics",
                "age",
            ],
            reference={"diagnosis": "nonIBD"},
            standardize=False,
        )

        self.assertEqual(results.results.query("qval.lt(0.25)").shape[0], 201)

    def test_flags_are_handled_correctly(self):
        results = py_utils.maaslin_utils.Maaslin(
            input_data=MaaslinTestCase.get_input_data(),
            input_metadata=MaaslinTestCase.get_input_metadata(),
            normalization="NONE",
            transform="NONE",
            analysis_method="ZINB",
            random_effects=["site", "subject"],
            fixed_effects=[
                "diagnosis",
                "dysbiosisnonIBD",
                "dysbiosisUC",
                "dysbiosisCD",
                "antibiotics",
                "age",
            ],
            reference={"diagnosis": "nonIBD"},
            standardize=False,
            min_abundance=0.0001,
            min_prevalence=0.05,
            min_variance=0.01,
        )
        self.assertEqual(
            results.results.query(
                "qval.lt(0.25) and metadata=='dysbiosisCD'"
            ).feature.to_list(),
            [
                "Escherichia.coli",
                "Roseburia.intestinalis",
                "Faecalibacterium.prausnitzii",
            ],
        )


if __name__ == "__main__":
    unittest.main()
