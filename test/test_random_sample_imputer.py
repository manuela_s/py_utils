#!/usr/bin/env python3

import time
import unittest

import numpy
import pandas
import py_utils.random_sample_imputer


class RandomSampleImputerTest(unittest.TestCase):
    def getDataSet(self, n_samples=100, n_features=1000, nan_fraction=0.1):
        data = pandas.DataFrame(numpy.random.rand(n_samples, n_features))
        data = data.stack().sample(frac=1-nan_fraction).unstack()
        return data

    def testNoNansLeft(self):
        data = self.getDataSet()
        transformed = py_utils.random_sample_imputer.RandomSampleImputer().fit_transform(data)
        self.assertFalse(transformed.isna().any(axis=None))

    def testKeptOriginalData(self):
        data = self.getDataSet()
        transformed = py_utils.random_sample_imputer.RandomSampleImputer().fit_transform(data)
        data_tall = data.stack()
        transformed_tall = transformed.stack()
        self.assertTrue(data_tall.equals(transformed_tall.reindex(data_tall.index)))

    def testPerformance1(self):
        data = self.getDataSet()
        before = time.time()
        py_utils.random_sample_imputer.RandomSampleImputer().fit_transform(data)
        self.assertLess(time.time() - before, 0.1)

    def testPerformance2(self):
        data = self.getDataSet(n_samples=1000, n_features=5000)
        before = time.time()
        py_utils.random_sample_imputer.RandomSampleImputer().fit_transform(data)
        self.assertLess(time.time() - before, 1)


if __name__ == '__main__':
    unittest.main()
