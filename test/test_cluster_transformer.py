#!/usr/bin/env python3

import unittest

import numpy
import pandas
import sklearn.cluster
import sklearn.datasets
import sklearn.ensemble

import py_utils.cluster_transformer


class MyTestCase(unittest.TestCase):
    def test_fit_transform(self):
        """Test that fit_transform works and returns data that matches underlying clustering"""
        bunch = sklearn.datasets.load_iris()
        expected_labels = sklearn.cluster.KMeans(n_clusters=3, random_state=0).fit(bunch.data).labels_
        ct = py_utils.cluster_transformer.ClusterTransformer(sklearn.cluster.KMeans(n_clusters=3, random_state=0))
        actual_labels = ct.fit_transform(pandas.DataFrame(bunch.data)).cluster.values
        numpy.testing.assert_array_equal(expected_labels, actual_labels)

    def test_random_state(self):
        """Test that we can pass random_state to ClusterTransform and it has the expected effect."""
        self.fail('Not implemented')

    def test_transform_new_data_iris(self):
        """Test that we can run .transform with new data."""
        bunch = sklearn.datasets.load_iris()
        train, test = sklearn.model_selection.train_test_split(pandas.DataFrame(bunch.data))
        ct = py_utils.cluster_transformer.ClusterTransformer(
            sklearn.cluster.KMeans(n_clusters=3, random_state=0),
            sklearn.ensemble.RandomForestClassifier(n_estimators=10, random_state=0))
        ct.fit_transform(train)
        ct.transform(test)

    def test_transform_new_data(self):
        """Test that .transform returns reasonable clusters for a dummy dataset"""
        # Create a dataset with obvious clustering
        X, y = sklearn.datasets.make_classification(
            n_features=2, n_redundant=0, n_clusters_per_class=1, class_sep=10, random_state=0)
        X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(
            pandas.DataFrame(X), pandas.Series(y))
        ct = py_utils.cluster_transformer.ClusterTransformer(
            sklearn.cluster.KMeans(n_clusters=2, random_state=0),
            sklearn.ensemble.RandomForestClassifier(n_estimators=10, random_state=0))
        clusters_train = ct.fit_transform(X_train)
        clusters_test = ct.transform(X_test)
        # Verify that all training data from each class got assigned to a single cluster:
        self.assertEqual(len(clusters_train.cluster[y_train == 0].unique()), 1)
        self.assertEqual(len(clusters_train.cluster[y_train == 1].unique()), 1)
        # Verify that all testing data from each class got assigned to a single cluster:
        self.assertEqual(len(clusters_test.cluster[y_test == 0].unique()), 1)
        self.assertEqual(len(clusters_test.cluster[y_test == 1].unique()), 1)
        # Verify that the testing data in class 0 got assign the same cluster as the training data in class 0
        numpy.testing.assert_array_equal(clusters_test.cluster[y_test == 0].unique(),
                                         clusters_train.cluster[y_train == 0].unique())
        # Verify that the testing data in class 1 got assign the same cluster as the training data in class 1
        numpy.testing.assert_array_equal(clusters_test.cluster[y_test == 1].unique(),
                                         clusters_train.cluster[y_train == 1].unique())


if __name__ == '__main__':
    unittest.main()
