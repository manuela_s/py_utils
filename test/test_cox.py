#!/usr/bin/env python3

import abc
import io
import os
import pathlib
import tempfile
import unittest

import matplotlib.pyplot
import numpy
import pandas
import pandas.core.dtypes.dtypes

import py_utils.cox


def compare_to_golden_file(
    test: unittest.TestCase, result: str, golden_filename: str
) -> None:
    """
    Helper to compare the result of a unit test to an expected result saved in a 'golden_file'.
    :param test: unit test to check results for;
    :param result: actual result from unit test;
    :param golden_filename: name of file with expected result.
    :raises AssertionError if actual and expected results do not match.
    """
    golden_filename = pathlib.Path(golden_filename)
    try:
        golden = golden_filename.read_text()
        test.assertEqual(golden, result)
    except (AssertionError, FileNotFoundError) as exc:
        with tempfile.NamedTemporaryFile(
            "w", suffix=golden_filename.suffix, delete=False
        ) as tmpfile:
            tmpfile.write(result)
            raise AssertionError(
                "Did not match golden file {}. Results saved in {}".format(
                    golden_filename, tmpfile.name
                )
            ) from exc


class TestHazardRatioCI(unittest.TestCase):
    def setUp(self):
        # Set reproducible hash for saving svg files.
        matplotlib.rcParams["svg.hashsalt"] = "TEST"
        os.environ['SOURCE_DATE_EPOCH'] = "0"

    def test_to_svg(self):
        hr = py_utils.cox.HazardRatioCI(0.5, 0.4, 0.7)
        svg = hr.to_svg()

        compare_to_golden_file(self, svg, "hazard_ratio_ci_golden.svg")


class AbstractTestCoxModel(abc.ABC, unittest.TestCase):
    """Common unittests for UnivariateCoxPerVariable and MultivariateCox classes."""

    def setUp(self):
        self.full_data = pandas.read_csv(
            "lung.csv",
            dtype={
                "ph.karno": pandas.CategoricalDtype(
                    ["50", "60", "70", "80", "90", "100"]
                ),
                "sex": pandas.CategoricalDtype(["1", "2"]),
            },
        )

        self.full_data["status"] = self.full_data.status == 2

        self.data = self.full_data[["time", "status", "age", "sex", "ph.karno"]]

        # Set reproducible hash for saving svg files.
        matplotlib.rcParams["svg.hashsalt"] = "TEST"

    def get_cox(self):
        """Helper to get fitted Cox regression model."""
        cox = self.cox_class
        cox.fit(self.data, duration_col="time", event_col="status", drop_na=True)

        return cox

    def test_get_reference_level(self):
        cox = self.get_cox()

        self.assertEqual(None, cox.get_reference_level("age"))
        self.assertEqual("1", cox.get_reference_level("sex"))
        self.assertEqual("50", cox.get_reference_level("ph.karno"))

    def test_missing_data(self):
        cox = self.cox_class
        self.assertRaises(
            ValueError, cox.fit, self.data, duration_col="time", event_col="status"
        )

    def test_summary(self):
        cox = self.get_cox()

        golden = pandas.read_csv(
            self.golden_summary,
            dtype={
                "model": pandas.core.dtypes.dtypes.CategoricalDtype(
                    self.model_categories
                ),
                "term": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["age", "sex", "ph.karno"]
                ),
                "term_level": pandas.core.dtypes.dtypes.CategoricalDtype(
                    [
                        "age",
                        "sex_1",
                        "sex_2",
                        "ph.karno_50",
                        "ph.karno_60",
                        "ph.karno_70",
                        "ph.karno_80",
                        "ph.karno_90",
                        "ph.karno_100",
                    ]
                ),
                "ref_level": "category",
                "level": "category",
                "per_level_n_total": "Int64",
                "per_model_n_total": "Int64",
                "per_model_n_events": "Int64",
            },
            index_col=["model", "term", "term_level", "ref_level", "level"],
        )

        pandas.testing.assert_frame_equal(
            golden, cox.summary, check_exact=False, check_less_precise=True
        )


class TestUnivariateCoxPerVariable(AbstractTestCoxModel):

    cox_class = py_utils.cox.UnivariateCoxPerVariable()
    golden_summary = "golden_cox_summary.csv"
    model_categories = ["age", "sex", "ph.karno"]

    def test_get_n_per_level(self):
        cox = self.cox_class
        cox.fit(self.full_data, duration_col="time", event_col="status", drop_na=True)

        self.assertEqual(228, cox.get_n_per_level("age", None))
        self.assertEqual(90, cox.get_n_per_level("sex", "2"))
        self.assertEqual(19, cox.get_n_per_level("ph.karno", "60"))
        self.assertEqual(32, cox.get_n_per_level("ph.karno", "70"))
        self.assertEqual(67, cox.get_n_per_level("ph.karno", "80"))
        self.assertEqual(74, cox.get_n_per_level("ph.karno", "90"))
        self.assertEqual(29, cox.get_n_per_level("ph.karno", "100"))

        self.assertEqual(181, cox.get_n_per_level("meal.cal", None))

    def test_summary_per_model(self):
        cox = self.get_cox()

        # Check selected outputs with values manually extracted from R
        self.assertIsInstance(cox.summary_per_model.index, pandas.CategoricalIndex)
        self.assertListEqual(
            ["age", "sex", "ph.karno"], cox.summary_per_model.index.to_list()
        )
        self.assertAlmostEqual(
            0.03946128, cox.summary_per_model.loc["age", "pvalue_lr"], places=5
        )
        self.assertAlmostEqual(
            0.55023983, cox.summary_per_model.loc["age", "c_index"], places=5
        )
        self.assertEqual(228, cox.summary_per_model.loc["age", "n_total"])
        self.assertEqual(165, cox.summary_per_model.loc["age", "n_events"])

        self.assertAlmostEqual(
            0.03986437, cox.summary_per_model.loc["ph.karno", "pvalue_lr"], places=5
        )
        self.assertAlmostEqual(
            0.59242976, cox.summary_per_model.loc["ph.karno", "c_index"], places=5
        )
        self.assertEqual(227, cox.summary_per_model.loc["ph.karno", "n_total"])
        self.assertEqual(164, cox.summary_per_model.loc["ph.karno", "n_events"])

    def test_summary_per_term(self):
        cox = self.get_cox()

        self.assertIsInstance(
            cox.summary_per_term.index.get_level_values("model"),
            pandas.CategoricalIndex,
        )
        self.assertIsInstance(
            cox.summary_per_term.index.get_level_values("term"), pandas.CategoricalIndex
        )

        # Check selected outputs with values manually extracted from R
        self.assertListEqual(
            ["age", "sex", "ph.karno"],
            cox.summary_per_term.index.get_level_values("model").to_list(),
        )
        self.assertListEqual(
            ["age", "sex", "ph.karno"],
            cox.summary_per_term.index.get_level_values("term").to_list(),
        )
        self.assertAlmostEqual(
            0.03946128, cox.summary_per_term.loc[("age", "age"), "pvalue_lr"], places=5
        )

    def test_summary_per_level(self):
        cox = self.get_cox()

        # Check selected outputs
        self.assertIsInstance(
            cox.summary_per_level.index.get_level_values("model"),
            pandas.CategoricalIndex,
        )
        self.assertIsInstance(
            cox.summary_per_level.index.get_level_values("term"),
            pandas.CategoricalIndex,
        )
        self.assertIsInstance(
            cox.summary_per_level.index.get_level_values("term_level"),
            pandas.CategoricalIndex,
        )
        self.assertIsInstance(
            cox.summary_per_level.index.get_level_values("level"),
            pandas.CategoricalIndex,
        )

        golden_summary_index = [
            ("age", "age", "age", numpy.nan),
            ("sex", "sex", "sex_2", "2"),
            ("ph.karno", "ph.karno", "ph.karno_60", "60"),
            ("ph.karno", "ph.karno", "ph.karno_70", "70"),
            ("ph.karno", "ph.karno", "ph.karno_80", "80"),
            ("ph.karno", "ph.karno", "ph.karno_90", "90"),
            ("ph.karno", "ph.karno", "ph.karno_100", "100"),
        ]
        self.assertListEqual(
            golden_summary_index, cox.summary_per_level.index.to_list()
        )
        self.assertAlmostEqual(
            0.01872018,
            cox.summary_per_level.loc[("age", "age", "age", numpy.nan), "coef"],
            places=5,
        )
        self.assertAlmostEqual(
            -0.5310108530989734,
            cox.summary_per_level.loc[("sex", "sex", "sex_2", "2"), "coef"],
            places=5,
        )
        self.assertAlmostEqual(
            0.66772196,
            cox.summary_per_level.loc[
                ("ph.karno", "ph.karno", "ph.karno_60", "60"), "coef"
            ],
            places=4,
        )
        self.assertAlmostEqual(
            0.49611933,
            cox.summary_per_level.loc[
                ("ph.karno", "ph.karno", "ph.karno_70", "70"), "coef"
            ],
            places=5,
        )
        self.assertAlmostEqual(
            0.23832040370193222,
            cox.summary_per_level.loc[
                ("ph.karno", "ph.karno", "ph.karno_80", "80"), "coef"
            ],
            places=5,
        )
        self.assertAlmostEqual(
            -0.03516954,
            cox.summary_per_level.loc[
                ("ph.karno", "ph.karno", "ph.karno_90", "90"), "coef"
            ],
            places=5,
        )
        self.assertAlmostEqual(
            -0.21959845574871417,
            cox.summary_per_level.loc[
                ("ph.karno", "ph.karno", "ph.karno_100", "100"), "coef"
            ],
            places=5,
        )

        self.assertEqual(
            67,
            cox.summary_per_level.loc[
                ("ph.karno", "ph.karno", "ph.karno_80", "80"), "n_total"
            ],
        )

    def test_plot(self):
        cox = self.get_cox()

        cox.plot()
        fh = io.StringIO()
        matplotlib.pyplot.savefig(fh, format="svg")

        compare_to_golden_file(self, fh.getvalue(), "cox_golden.svg")

    def test_html(self):
        cox = self.get_cox()

        fh = io.StringIO()
        cox.to_html(fh, uuid="TEST")

        compare_to_golden_file(self, fh.getvalue(), "cox_golden.html")


class TestMultivariateCox(AbstractTestCoxModel):

    cox_class = py_utils.cox.MultivariateCox()
    golden_summary = "golden_cox_summary_multi.csv"
    model_categories = ["multi"]

    def test_per_model_summary(self):
        cox = self.get_cox()

        self.assertEqual(1, cox.summary_per_model.shape[0])
        self.assertIsInstance(cox.summary_per_model.index, pandas.CategoricalIndex)

        self.assertAlmostEqual(
            5.275473e-04, cox.summary_per_model.pvalue_lr[0], places=5
        )
        self.assertAlmostEqual(0.63045785, cox.summary_per_model.c_index[0], places=4)
        self.assertEqual(227, cox.summary_per_model.n_total[0])
        self.assertEqual(164, cox.summary_per_model.n_events[0])

    def test_per_term_summary(self):
        cox = self.get_cox()

        self.assertEqual(3, cox.summary_per_term.shape[0])
        self.assertEqual(None, cox.summary_per_term.loc[("multi", "age"), "ref_level"])
        self.assertEqual("1", cox.summary_per_term.loc[("multi", "sex"), "ref_level"])
        self.assertEqual(
            "50", cox.summary_per_term.loc[("multi", "ph.karno"), "ref_level"]
        )

        self.assertAlmostEqual(
            0.1288912, cox.summary_per_term.loc[("multi", "age"), "pvalue_lr"], places=5
        )
        self.assertAlmostEqual(
            0.0005534, cox.summary_per_term.loc[("multi", "sex"), "pvalue_lr"], places=5
        )
        self.assertAlmostEqual(
            0.0340637,
            cox.summary_per_term.loc[("multi", "ph.karno"), "pvalue_lr"],
            places=5,
        )

    def test_per_level_summary(self):
        cox = self.get_cox()

        self.assertEqual(7, cox.summary_per_level.shape[0])
        self.assertListEqual(
            ["model", "term", "term_level", "level"], cox.summary_per_level.index.names
        )

        self.assertAlmostEqual(
            1.0142641,
            cox.summary_per_level.loc[("multi", "age", "age", None), "exp(coef)"],
            places=5,
        )
        self.assertAlmostEqual(
            0.5653038469301483,
            cox.summary_per_level.loc[("multi", "sex", "sex_2", "2"), "exp(coef)"],
            places=5,
        )
        self.assertAlmostEqual(
            2.4912071,
            cox.summary_per_level.loc[
                ("multi", "ph.karno", "ph.karno_60", "60"), "exp(coef)"
            ],
            places=4,
        )
        self.assertAlmostEqual(
            2.239269828504722,
            cox.summary_per_level.loc[
                ("multi", "ph.karno", "ph.karno_70", "70"), "exp(coef)"
            ],
            places=5,
        )
        self.assertAlmostEqual(
            1.7783212116350262,
            cox.summary_per_level.loc[
                ("multi", "ph.karno", "ph.karno_80", "80"), "exp(coef)"
            ],
            places=5,
        )
        self.assertAlmostEqual(
            1.3186068,
            cox.summary_per_level.loc[
                ("multi", "ph.karno", "ph.karno_90", "90"), "exp(coef)"
            ],
            places=5,
        )
        self.assertAlmostEqual(
            1.0763282799050284,
            cox.summary_per_level.loc[
                ("multi", "ph.karno", "ph.karno_100", "100"), "exp(coef)"
            ],
            places=5,
        )

        self.assertEqual(
            227, cox.summary_per_level.loc[("multi", "age", "age", None), "n_total"]
        )
        self.assertEqual(
            90, cox.summary_per_level.loc[("multi", "sex", "sex_2", "2"), "n_total"]
        )
        self.assertEqual(
            19,
            cox.summary_per_level.loc[
                ("multi", "ph.karno", "ph.karno_60", "60"), "n_total"
            ],
        )
        self.assertEqual(
            32,
            cox.summary_per_level.loc[
                ("multi", "ph.karno", "ph.karno_70", "70"), "n_total"
            ],
        )
        self.assertEqual(
            67,
            cox.summary_per_level.loc[
                ("multi", "ph.karno", "ph.karno_80", "80"), "n_total"
            ],
        )
        self.assertEqual(
            74,
            cox.summary_per_level.loc[
                ("multi", "ph.karno", "ph.karno_90", "90"), "n_total"
            ],
        )
        self.assertEqual(
            29,
            cox.summary_per_level.loc[
                ("multi", "ph.karno", "ph.karno_100", "100"), "n_total"
            ],
        )


class TestCoxSummaryTable(unittest.TestCase):
    def setUp(self):
        self.full_data = pandas.read_csv(
            "lung.csv",
            dtype={
                "ph.karno": pandas.CategoricalDtype(
                    ["50", "60", "70", "80", "90", "100"]
                ),
                "sex": pandas.CategoricalDtype(["1", "2"]),
            },
        )

        self.full_data["status"] = self.full_data.status == 2

        self.data = self.full_data[["time", "status", "age", "sex", "ph.karno"]]

        # Set reproducible hash for saving svg files.
        matplotlib.rcParams["svg.hashsalt"] = "TEST"
        numpy.random.seed(1)
        os.environ['SOURCE_DATE_EPOCH'] = "0"

    def test_variables(self):
        cox_summary = py_utils.cox.CoxSummaryTable(self.data)
        cox_summary.add_endpoint("time", "status", "OS")
        self.assertListEqual(["age", "sex", "ph.karno"], cox_summary.variables)

    def test_variables2(self):
        full_data = self.full_data.copy()
        # Synthesise additional survival end-point
        full_data["time2"] = full_data.time.sample(full_data.shape[0]).values
        full_data["status2"] = full_data.status.sample(full_data.shape[0]).values

        cox_summary = py_utils.cox.CoxSummaryTable(full_data)
        cox_summary.add_endpoint("time", "status", "OS")
        cox_summary.add_endpoint("time2", "status2", "OS2")
        self.assertListEqual(
            [
                "inst",
                "age",
                "sex",
                "ph.ecog",
                "ph.karno",
                "pat.karno",
                "meal.cal",
                "wt.loss",
            ],
            cox_summary.variables,
        )

    def test_univariate_only_summary(self):
        cox_summary = py_utils.cox.CoxSummaryTable(self.data)
        cox_summary.add_endpoint("time", "status", "OS")
        cox_summary.fit(drop_na=True)
        golden = pandas.read_csv(
            "golden_cox_summary_table_uni1.csv",
            dtype={
                "endpoint": pandas.core.dtypes.dtypes.CategoricalDtype(["OS"]),
                "model_type": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["univariate"]
                ),
                "model": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["age", "sex", "ph.karno"]
                ),
                "term": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["age", "sex", "ph.karno"]
                ),
                "term_level": pandas.core.dtypes.dtypes.CategoricalDtype(
                    [
                        "age",
                        "sex_2",
                        "ph.karno_60",
                        "ph.karno_70",
                        "ph.karno_80",
                        "ph.karno_90",
                        "ph.karno_100",
                    ]
                ),
                "ref_level": "category",
                "level": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["2", "60", "70", "80", "90", "100"]
                ),
                "per_level_n_total": "Int64",
                "per_model_n_total": "Int64",
                "per_model_n_events": "Int64",
            },
            index_col=[
                "endpoint",
                "model_type",
                "model",
                "term",
                "term_level",
                "ref_level",
                "level",
            ],
        )

        pandas.testing.assert_frame_equal(
            golden, cox_summary.summary, check_exact=False, check_less_precise=True
        )

    def test_univariate_only_summary_2_endpoints(self):
        # Synthesise additional survival end-point
        data = self.data.copy()
        data["time2"] = data.time.sample(data.shape[0]).values
        data["status2"] = data.status.sample(data.shape[0]).values
        cox_summary = py_utils.cox.CoxSummaryTable(data)
        cox_summary.add_endpoint("time", "status", "OS")
        cox_summary.add_endpoint("time2", "status2", "OS2")
        cox_summary.fit(drop_na=True)
        golden = pandas.read_csv(
            "golden_cox_summary_table_uni2.csv",
            dtype={
                "endpoint": pandas.core.dtypes.dtypes.CategoricalDtype(["OS", "OS2"]),
                "model_type": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["univariate"]
                ),
                "model": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["age", "sex", "ph.karno"]
                ),
                "term": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["age", "sex", "ph.karno"]
                ),
                "term_level": pandas.core.dtypes.dtypes.CategoricalDtype(
                    [
                        "age",
                        "sex_2",
                        "ph.karno_60",
                        "ph.karno_70",
                        "ph.karno_80",
                        "ph.karno_90",
                        "ph.karno_100",
                    ]
                ),
                "ref_level": "category",
                "level": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["2", "60", "70", "80", "90", "100"]
                ),
                "per_level_n_total": "Int64",
                "per_model_n_total": "Int64",
                "per_model_n_events": "Int64",
            },
            index_col=[
                "endpoint",
                "model_type",
                "model",
                "term",
                "term_level",
                "ref_level",
                "level",
            ],
        )

        pandas.testing.assert_frame_equal(
            golden, cox_summary.summary, check_exact=False, check_less_precise=True
        )

    def test_univariate_and_multivariate(self):
        cox_summary = py_utils.cox.CoxSummaryTable(self.data)
        cox_summary.add_multivariate_model(["age", "sex"], "MV1")

        cox_summary.add_endpoint("time", "status", "OS")
        cox_summary.fit(drop_na=True)

        golden = pandas.read_csv(
            "golden_cox_summary_table_multi1.csv",
            dtype={
                "endpoint": pandas.core.dtypes.dtypes.CategoricalDtype(["OS"]),
                "model_type": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["univariate", "MV1"]
                ),
                "model": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["age", "sex", "ph.karno", "multi"]
                ),
                "term": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["age", "sex", "ph.karno"]
                ),
                "term_level": pandas.core.dtypes.dtypes.CategoricalDtype(
                    [
                        "age",
                        "sex_2",
                        "ph.karno_60",
                        "ph.karno_70",
                        "ph.karno_80",
                        "ph.karno_90",
                        "ph.karno_100",
                    ]
                ),
                "ref_level": "category",
                "level": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["2", "60", "70", "80", "90", "100"]
                ),
                "per_level_n_total": "Int64",
                "per_model_n_total": "Int64",
                "per_model_n_events": "Int64",
            },
            index_col=[
                "endpoint",
                "model_type",
                "model",
                "term",
                "term_level",
                "ref_level",
                "level",
            ],
        )

        pandas.testing.assert_frame_equal(
            golden, cox_summary.summary, check_exact=False, check_less_precise=True
        )

    def test_univariate_and_multivariate2(self):
        data = self.data.copy()
        data["time2"] = data.time.sample(data.shape[0]).values
        data["status2"] = data.status.sample(data.shape[0]).values
        cox_summary = py_utils.cox.CoxSummaryTable(data)
        cox_summary.add_endpoint("time", "status", "OS")
        cox_summary.add_endpoint("time2", "status2", "OS2")
        cox_summary.add_multivariate_model(["age", "sex"], "MV1")
        cox_summary.add_multivariate_model(["age", "sex", "ph.karno"], "MV2")

        cox_summary.fit(drop_na=True)

        golden = pandas.read_csv(
            "golden_cox_summary_table_multi2.csv",
            dtype={
                "endpoint": pandas.core.dtypes.dtypes.CategoricalDtype(["OS", "OS2"]),
                "model_type": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["univariate", "MV1", "MV2"]
                ),
                "model": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["age", "sex", "ph.karno", "multi"]
                ),
                "term": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["age", "sex", "ph.karno"]
                ),
                "term_level": pandas.core.dtypes.dtypes.CategoricalDtype(
                    [
                        "age",
                        "sex_2",
                        "ph.karno_60",
                        "ph.karno_70",
                        "ph.karno_80",
                        "ph.karno_90",
                        "ph.karno_100",
                    ]
                ),
                "ref_level": "category",
                "level": pandas.core.dtypes.dtypes.CategoricalDtype(
                    ["2", "60", "70", "80", "90", "100"]
                ),
                "per_level_n_total": "Int64",
                "per_model_n_total": "Int64",
                "per_model_n_events": "Int64",
            },
            index_col=[
                "endpoint",
                "model_type",
                "model",
                "term",
                "term_level",
                "ref_level",
                "level",
            ],
        )

        pandas.testing.assert_frame_equal(
            golden, cox_summary.summary, check_exact=False, check_less_precise=True
        )

    def test_univariate_and_multivariate2_html(self):
        data = self.data.copy()
        data["time2"] = data.time.sample(data.shape[0]).values
        data["status2"] = data.status.sample(data.shape[0]).values
        cox_summary = py_utils.cox.CoxSummaryTable(data)
        cox_summary.add_endpoint("time", "status", "OS")
        cox_summary.add_endpoint("time2", "status2", "OS2")
        cox_summary.add_multivariate_model(["age", "sex"], "MV1")
        cox_summary.add_multivariate_model(["age", "sex", "ph.karno"], "MV2")

        cox_summary.fit(drop_na=True)

        fh = io.StringIO()
        cox_summary.to_html(fh, uuid="TEST")

        compare_to_golden_file(
            self, fh.getvalue(), "golden_cox_summary_table_multi2.html"
        )


# Hide abstract class to avoid testing it.
del AbstractTestCoxModel

if __name__ == "__main__":
    unittest.main()
