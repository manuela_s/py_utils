#!/usr/bin/env python3

import pandas
import numpy
import sklearn.base


class RandomSampleImputer(sklearn.base.BaseEstimator, sklearn.base.TransformerMixin):
    """Imputer that replaces missing data with values randomly selected from existing values."""

    def fit(self, X):
        # Store the values of X to use as replacement for NaNs in transform()
        # We store the values sorted per feature, so picking a random non-nan value can be done easily by looking
        # up the value in one of the first num_values rows for each feature
        self.values_ = X.values.copy()
        self.values_.sort(axis=0)
        self.num_values_ = X.notna().sum().values
        return self

    def transform(self, X):
        # For every data-point the row index of one of the non-nan values in self.values
        rand_ind = (numpy.random.rand(*X.shape) * self.num_values_).astype(int)
        # Create a numpy-array with a random non-nan replacement value for every data-point
        rand_values = numpy.take_along_axis(self.values_, rand_ind, axis=0)

        # Return new dataframe with NaNs replaced with corresponding values from rand_values
        return X.where(X.notna(), rand_values)
