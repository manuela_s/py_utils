#!/usr/bin/env python3

import matplotlib.ticker
import numpy


class OutlierLogLocator(matplotlib.ticker.SymmetricalLogLocator):
    """Class to scale axes with data outliers.
    Use linear scale in the area with most data-points and log scale to cover extreme outliers.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._linear_locator = matplotlib.ticker.AutoLocator()

    def tick_values(self, vmin: float, vmax: float) -> numpy.ndarray:
        linear_range_ticks = self._linear_locator.tick_values(
            vmin, min(vmax, self._linthresh)
        )
        log_range_ticks = super().tick_values(self._linthresh, vmax)

        if vmax <= self._linthresh:
            return linear_range_ticks
        else:
            return numpy.concatenate(
                [
                    linear_range_ticks[linear_range_ticks < self._linthresh],
                    log_range_ticks[log_range_ticks > self._linthresh],
                ]
            )
