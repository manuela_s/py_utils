#!/usr/bin/env python3

"""
Functions to make variable width stacked barplots
"""

import pandas
import typing
import matplotlib.pyplot


def barh(
    df: pandas.DataFrame,
    heights: pandas.Series,
    color: typing.Union[typing.Sequence, dict] = None,
    y_offset: float = 5,
    ax=None,
    **kwargs,
):
    """Make horizaontal stacked barplot with variable height.
    :param df: data to plot. one row per horizontal bar, one column per stacked part;
    :param heights: the height of the bars. index should match df;
    :param color: array_like, or dict, optional. The color for each of the DataFrame’s columns;
    :param y_offset: distance between bars;
    :param ax: axes to plot in. Default to plot in gca;
    :param kwargs: key/value arguments passed down to 'matplotlib.pyplot.barh'.
    """
    assert df.index.equals(heights.index)
    if isinstance(color, typing.Sequence):
        color = dict(zip(df.columns, color))

    y_start = heights.add(y_offset).cumsum().shift(fill_value=0)
    y_end = y_start + heights
    y_center = (y_start + y_end) / 2

    left = df.cumsum(axis=1).shift(axis=1, fill_value=0)

    if ax is None:
        ax = matplotlib.pyplot.gca()

    for col in df.columns:
        ax.barh(
            y=y_start,
            width=df[col],
            height=heights,
            left=left[col],
            align="edge",
            color=color[col],
            label=col,
            **kwargs,
        )

    ax.set_yticks(y_center)
    ax.set_yticklabels(df.index)
    ax.set_ylim(0, y_end.iloc[-1])
    ax.set_xlabel(df.index.name)
    ax.set_ylabel(df.columns.name)
