#!/usr/bin/env python3

import sklearn.base
import pandas

class PandasTransformer(sklearn.base.BaseEstimator, sklearn.base.TransformerMixin):
    """Transformer that runs encapsulates another transformer, but exposes results as pandas dataframe"""
    def __init__(self, base_estimator, feature_prefix='F'):
        assert(hasattr(base_estimator, 'fit_transform'))
        self.base_estimator = base_estimator
        self.feature_prefix = feature_prefix

    def fit(self, X, y=None):
        if y is None:
            output = self.base_estimator.fit_transform(X)
        else:
            output = self.base_estimator.fit_transform(X, y)
        self.fallback_feature_names_ = ['{}{}'.format(self.feature_prefix, i) for i in range(output.shape[1])]
        return self

    def get_feature_names(self):
        if hasattr(self.base_estimator, 'get_feature_names'):
            return self.base_estimator.get_feature_names()
        else:
            return self.fallback_feature_names_

    def _process_output(self, X, output):
        return pandas.DataFrame(output, index=X.index, columns=self.get_feature_names())

    def transform(self, X):
        output = self.base_estimator.transform(X)
        return self._process_output(X, output)

    def fit_transform(self, X, y=None):
        output = self.base_estimator.fit_transform(X)
        self.fallback_feature_names_ = ['{}{}'.format(self.feature_prefix, i) for i in range(output.shape[1])]
        return self._process_output(X, output)
