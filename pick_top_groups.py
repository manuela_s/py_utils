#!/usr/bin/env python3

def pick_top_groups(data, max_n_groups=8, min_threshold_value=1):
    """
    Helper function to pick top groups.
    Filter the data to at most n groups (+ "other") (rows) with the highest max value and reorder the groups by value.
    :param data: dataframe with groups as rows.
    :param max_n_groups: highest number of groups to return
    :param min_threshold_value: only keep groups with max values higher than threshold.
    :return: dataframe, filtered for n biggest groups
    """
    data = data.copy()

    groups_to_keep = data.max().nlargest(max_n_groups)
    groups_to_keep = groups_to_keep[groups_to_keep > min_threshold_value]

    cols_to_combine = set(data.columns).difference(groups_to_keep.index)

    if len(cols_to_combine) > 1:
        # Combine remaining columns into "other"
        data['other'] = data[set(data.columns).difference(groups_to_keep.index)].sum(axis=1)
        return data[list(groups_to_keep.index) + ['other']]
    else:
        # Keep original cols if there are 0 or 1 cols to combine.
        return data[list(groups_to_keep.index) + list(cols_to_combine)]
