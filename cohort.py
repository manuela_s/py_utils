#!/usr/bin/env python3

"""
Common functionality for dealing with patient cohorts
"""

import abc
import itertools
from typing import List, Sequence, Optional

import pandas
import sklearn.preprocessing

import pathseq_utils


def standardization_helper(data: pandas.DataFrame) -> pandas.DataFrame:
    """
    Standardize RNA dataframe
    :param data:
    :return:
    """

    return pandas.DataFrame(
        sklearn.preprocessing.RobustScaler().fit_transform(data),
        index=data.index,
        columns=data.columns,
    )


def colossus_like_helper(data: pandas.DataFrame) -> pandas.DataFrame:
    data["ras_status"] = (
        data[["kras_status", "nras_status", "hras_status", "mras_status"]]
        .eq("wild_type")
        .all(axis=1)
        .map({True: "wild_type", False: "mutant"})
    )
    data["msi_ras"] = (
        data.microsatellite_status.astype("str") + "_" + data.ras_status.astype("str")
    )

    data["colossus_like"] = pandas.Categorical(
        data.msi_ras.replace(
            {
                "MSS_wild_type": "no",
                "MSS_mutant": "yes",
                "MSI_wild_type": "no",
                "MSI_mutant": "no",
                "nan_wild_type": "no",
                "nan_mutant": None,
            }
        ),
        ["no", "yes"],
    )

    return data


class Cohort(abc.ABC):
    @abc.abstractmethod
    def get_clinical(self) -> pandas.DataFrame:
        """Get clinical data
        :return: dataframe with one row per patient, index patient_id and columns:
                 - os_months: float
                 - os_event: float (0.0 no event, 1.0 event, NaN missing)
                 - dss_months
                 - dss_event
                 - dfs_months
                 - dfs_event
                 - sex, categorical, male/female
                 - age, Int64
                 - cancer_type, categorical, COAD/READ
                 - resection_margins, categorical, R0/R1_or_R2
                 - stage, categorical, 1/2/3/4
                 - has_lymphovascular_invasion, categorical, no/yes
        """

    @abc.abstractmethod
    def get_rna(self, standardization: bool = True) -> pandas.DataFrame:
        """Get RNA gene expression
        :param rna_ids: What style of rna ids to use. Can be symbol, ensembl_id or entrezgene.
        :param standardization: if True, return gene expression as gausion-like distribution with mean of 0 and std dev of 1.
               If False, return the original data.
        :return: dataframe with one row per patient, index patient_id and one
        """

    @abc.abstractmethod
    def get_subtypes(self) -> pandas.DataFrame:
        """Get subtype information for the patients in the cohort.
        :return: dataframe with one row per patient, index patient_id, and columns:
                 - 'CMS': categorical, ['CMS1', 'CMS2', 'CMS3', 'CMS4', 'NOLBL']
                 - 'CRIS': categorical, ['CRIS-A', 'CRIS-B', 'CRIS-C', 'CRIS-D', 'CRIS-E', 'NOLBL']
                 - 'is_mesenchymal': categorical, ['yes', 'no', 'NOLBL']
        """

    @abc.abstractmethod
    def get_immuno_composition(self) -> pandas.DataFrame:
        """Get RNA gene expression
        :return: dataframe with one row per patient, index patient_id and one column per immuno cell type
        """

    @abc.abstractmethod
    def get_microbe_sample_metadata(self) -> pandas.DataFrame:
        pass

    def _get_microbes_tax_ids(
            self,
            tax_ids: List[str],
            metric: str = 'host_normalized_load',
            tissue: str = "tumour",
            per_distance: bool = False,
            include_technology: bool = False,
            per_sample: bool = False,
            decontaminated: bool = True,
    ):
        # Selecting samples:
        mapping = self.get_microbe_sample_metadata()
        mapping["threshold"] = mapping.technology.map({"rna": 10e6, "shallow_seq": 1e6})
        filter_metrics = pathseq_utils.get_filter_metrics()
        metadata = mapping.join(filter_metrics, how="inner")
        if tissue:
            metadata.query("tissue==@tissue", inplace=True)
        metadata.query("primary_reads > threshold", inplace=True)

        df = pathseq_utils.get_microbes(
            metric, list(metadata.index), tax_ids, decontaminated
        )

        groupby_cols = ["cohort", "patient_id"]
        if not tissue:
            groupby_cols.append("tissue")
        if per_distance:
            groupby_cols.append("distance")
        if include_technology:
            groupby_cols.append("technology")
        if per_sample:
            groupby_cols.append("sample_id")
        df = df.join(mapping[list(set(groupby_cols).intersection(mapping.columns))]).groupby(groupby_cols).mean()

        return df

    def get_microbes(
        self,
        taxonomic_rank: str,
        metric: str = 'host_normalized_load',
        kingdom: str = None,
        tissue: Optional[str] = "tumour",
        per_distance: bool = False,
        include_technology: bool = False,
        per_sample: bool = False,
        decontaminated: bool = True,
        include_tax_id_in_columns: bool = False,
    ) -> pandas.DataFrame:
        """
        Get microbes host normalized load or relative abundance.
        :param taxonomic_rank: e.g. "kingdom", "genus", "species", ...
        :param metric: Either 'host_normalized_load' or 'score_normalized'.
        :param kingdom: Subset to this super-kingdom (one of "bacteria", "fungi" or "virus"), or None for all.
        :param tissue: Subset to this tissue type (one of "tumour", "matched", "normal") or None for all. Defaults to "tumour".
        :param per_distance: Report microbes separately per distance. Defaults to False.
        :param include_technology: Include technology as index in returned dataframe. Defaults to False.
        :param per_sample: Report microbes separately per sample. Defaults to False.
        :param decontaminated: Report microbes after having removed contamination. Defaults to True.
        :return: dataframe indexed by 'cohort', 'patient_id' ('tissue', 'distance', 'technology', 'sample_id').
        """
        # Selecting tax_ids:
        taxonomy = pathseq_utils.get_taxonomy_metadata()
        taxonomy.query("type==@taxonomic_rank", inplace=True)
        if kingdom:
            taxonomy.query("kingdom==@kingdom", inplace=True)

        df = self._get_microbes_tax_ids(taxonomy.index.to_list(),
                                        metric=metric,
                                        tissue=tissue,
                                        per_distance=per_distance,
                                        include_technology=include_technology,
                                        per_sample=per_sample,
                                        decontaminated=decontaminated,
                                        )

        assert df.columns.equals(taxonomy.index)
        if kingdom:
            df.rename(columns=taxonomy.name, inplace=True)
            df.rename_axis(columns=taxonomic_rank, inplace=True)
        else:
            if include_tax_id_in_columns:
                df.columns = pandas.MultiIndex.from_frame(taxonomy.reset_index()[["kingdom", "name", "tax_id"]])
            else:
                df.columns = pandas.MultiIndex.from_frame(taxonomy[["kingdom", "name"]])
            df.rename_axis(columns={"name": taxonomic_rank}, inplace=True)
        return df

    # get_microbes(filter_rank='kingdom', filter_name='Bacteria', detailed_taxonomic_rank='Class') ->
    def get_microbes_hierarchy(
        self,
        detailed_taxonomic_rank,
        filter_taxonomic_rank=None,
        filter_microbe_name=None,
        metric: str = 'host_normalized_load',
        tissue: str = "tumour",
        per_distance: bool = False,
        include_technology: bool = False,
        decontaminated: bool = True,
    ) -> pandas.DataFrame:
        """
        Get microbes host normalized load or relative abundance.
        :param detailed_taxonomic_rank: Finest taxonomic rank to report microbes for. "kingdom", "genus", "species", ...
        :param filter_taxonomic_rank: Only include microbes for the given filter_microbe_name at the given filter_taxonomic_rank. E.g. "order"
        :param filter_microbe_name: Only include microbes for the given filter_microbe_name at the given filter_taxonomic_rank. E.g. "Fusobacteriales"
        :param metric: Either 'host_normalized_load' or 'score_normalized'.
        :param tissue: Subset to this tissue type (one of "tumour", "matched", "normal") or None for all. Defaults to "tumour".
        :param per_distance: Report microbes separately per distance. Defaults to False.
        :param include_technology: Include technology as index in returned dataframe. Defaults to False.
        :param decontaminated: Report microbes after having removed contamination. Defaults to True.
        :return: dataframe indexed by 'cohort', 'patient_id' ('tissue', 'distance', 'technology').
        """
        taxonomy = pathseq_utils.get_taxonomy_metadata()
        th = pathseq_utils.TaxonomyHierarchy(taxonomy)
        # Filter to the taxonomic ranks of interest:
        taxonomic_ranks = dict(itertools.islice(
            th.rank_columns.items(),
            [filter_taxonomic_rank in x for x in th.rank_columns.values()].index(True),
            [detailed_taxonomic_rank in x for x in th.rank_columns.values()].index(True)+1)
        )
        taxonomy = taxonomy.set_index(['type', 'name'], append=True)[[]].join(th.hierarchy_df[taxonomic_ranks.keys()])
        taxonomy.query('type.isin(@types)', local_dict={'types': itertools.chain(*taxonomic_ranks.values())}, inplace=True)
        if filter_taxonomic_rank and filter_microbe_name:
            taxonomy = taxonomy[taxonomy[filter_taxonomic_rank] == filter_microbe_name]
        elif filter_taxonomic_rank or filter_microbe_name:
            raise Exception("Can not specify filter_taxonomic_rank or filter_microbe_name without the other.")

        df = self._get_microbes_tax_ids(
            taxonomy.index.get_level_values('tax_id').to_list(),
            metric=metric,
            tissue=tissue,
            per_distance=per_distance,
            include_technology=include_technology,
            decontaminated=decontaminated,
        )
        assert df.columns.equals(taxonomy.index.get_level_values('tax_id'))
        df.columns = pandas.MultiIndex.from_frame(taxonomy.dropna(axis=1, how='all'))
        return df.sort_index(axis=1, na_position='first')


class CombinedCohort(Cohort):
    def __init__(self, cohorts: Sequence[Cohort]):
        self.cohorts = cohorts

    def get_clinical(self):
        cli = pandas.concat([cohort.get_clinical() for cohort in self.cohorts])
        return cli

    def get_cna(self) -> pandas.DataFrame:
        cna = pandas.concat([cohort.get_cna() for cohort in self.cohorts])

        return cna

    def get_rna(self, standardization: bool = True) -> pandas.DataFrame:
        return pandas.concat(
            [cohort.get_rna(standardization=standardization) for cohort in self.cohorts]
        )

    def get_rppa(self, standardization: bool = True) -> pandas.DataFrame:
        return pandas.concat(
            [
                cohort.get_rppa(standardization=standardization)
                for cohort in self.cohorts
            ]
        )

    def get_mutations(self) -> pandas.DataFrame:
        return pandas.concat([cohort.get_mutations() for cohort in self.cohorts])

    def get_mirna(self, standardization: bool = True) -> pandas.DataFrame:
        return pandas.concat(
            [
                cohort.get_mirna(standardization=standardization)
                for cohort in self.cohorts
            ]
        )

    def get_subtypes(self) -> pandas.DataFrame:
        return pandas.concat([cohort.get_subtypes() for cohort in self.cohorts])

    def get_immuno_composition(self) -> pandas.DataFrame:
        return pandas.concat(
            [cohort.get_immuno_composition() for cohort in self.cohorts]
        )

    def get_quantiseq_cell_types(self) -> pandas.DataFrame:
        return pandas.concat(
            [cohort.get_quantiseq_cell_types() for cohort in self.cohorts]
        )

    def get_rna_based_signatures(self) -> pandas.DataFrame:
        return pandas.concat(
            [cohort.get_rna_based_signatures() for cohort in self.cohorts]
        )

    def get_rppa_based_signatures(self) -> pandas.DataFrame:
        return pandas.concat(
            [cohort.get_rppa_based_signatures() for cohort in self.cohorts]
        )

    def get_microsatellite_and_mutational_status(self) -> pandas.DataFrame:
        data = pandas.concat(
            [
                cohort.get_microsatellite_and_mutational_status()
                for cohort in self.cohorts
            ]
        )
        data = colossus_like_helper(data)
        return data

    def get_microbe_sample_metadata(self) -> pandas.DataFrame:
        return pandas.concat(
            [cohort.get_microbe_sample_metadata() for cohort in self.cohorts]
        )


class SubCohort(Cohort):
    def __init__(self, cohort: Cohort, patient_ids: Sequence[str]):
        pass
