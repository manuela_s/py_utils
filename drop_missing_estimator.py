#!/usr/bin/env python3

import pandas
import sklearn.base

class DropMissingEstimator(sklearn.base.BaseEstimator):
    """Wrapper estimator that drops rows with missing x or y data before calling base estimator"""
    def __init__(self, base_estimator):
        assert(isinstance(base_estimator, sklearn.base.BaseEstimator))
        self.base_estimator = base_estimator

    def fit(self, X, y):
        # Fit base estimator after dropping rows with missing data
        assert(isinstance(X, pandas.DataFrame))
        assert(isinstance(y, (pandas.Series, pandas.DataFrame)))
        X_, y_ = X.dropna().align(y.dropna(), 'inner', axis=0)
        self.base_estimator.fit(X_, y_)
        return self

    def predict(self, X):
        return self.base_estimator.predict(X)

    def fit_predict(self, X, y):
        self.fit(X, y)
        return self.predict(X)

    def score(self, X, y):
        # Score base estimator after dropping rows with missing data
        assert(isinstance(X, pandas.DataFrame))
        assert(isinstance(y, (pandas.Series, pandas.DataFrame)))
        X, y = X.dropna().align(y.dropna(), 'inner', axis=0)
        return self.base_estimator.score(X, y)
