#!/usr/bin/env python3

import scipy.stats
import typing
import lifelines
import statsmodels.base.model


def _get_model_parameters(
    model: typing.Union[lifelines.CoxPHFitter, statsmodels.base.model.Model]
) -> typing.Tuple[float, int]:
    """
    Helper for compare_lr_test: get required parameters from model.
    :param model: statistical model to get parameters for;
    :return: tuple of (log likelihood, degrees of freedom).
    """
    if hasattr(model, "llf") and hasattr(model, "df_resid"):
        # For statsmodels models
        return model.llf, model.df_resid
    elif hasattr(model, "log_likelihood_") and hasattr(model, "params_"):
        return model.log_likelihood_, model.params_.shape[0]
    else:
        raise NotImplementedError(
            "Unable to get model parameters from model: ", str(model)
        )


def compare_lr_test(
    model: typing.Union[lifelines.CoxPHFitter, statsmodels.base.model.Model],
    restricted_model: typing.Union[lifelines.CoxPHFitter, statsmodels.base.model.Model],
) -> typing.Tuple[float, float, int]:
    """
    Generic likelihood ratio test between nested models.
    Based on https://github.com/statsmodels/statsmodels/issues/30 and RegressionResults.compare_lr_test.
    :param model: full model;
    :param restricted_model: the restricted model is assumed to be nested in the full model. The result instance of the
           restricted model is required to have two attributes, residual sum of squares (`ssr`) and residual degrees of
           freedom (`df_resid`);
    :return:
        lr_stat: likelihood ratio, chi-square distributed with df_diff degrees of freedom;
        p_value: p-value of the test statistic;
        df_diff: degrees of freedom of the restriction, i.e. difference in df between models.
    """
    llf_full, df_full = _get_model_parameters(model)
    llf_restr, df_restr = _get_model_parameters(restricted_model)

    lrstat = -2 * (llf_restr - llf_full)
    lrdf = abs(df_restr - df_full)
    lrpval = scipy.stats.chi2.sf(lrstat, lrdf)

    return lrstat, lrpval, lrdf
