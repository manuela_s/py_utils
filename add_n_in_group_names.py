#!/usr/bin/env python3

import pandas


def add_n_in_group_names(data: pandas.DataFrame, grouping_var: str) -> pandas.DataFrame:
    """
    Rename group names to include the n counts for the groups.
    :param data: dataframe with original data;
    :param grouping_var: name of the index to group by;
    :return: dataframe with n counts added for index named 'grouping_var'.
    """
    data = data.copy()
    sizes = data.groupby(grouping_var).size().to_frame("n")
    sizes["group"] = sizes.index
    sizes["new_names"] = sizes.apply(lambda x: f"{x.group} (n={x.n})", axis=1)
    data.index = pandas.MultiIndex.from_frame(
        data.index.to_frame().replace({grouping_var: sizes.new_names})
    )

    return data
