import matplotlib.colors
import colorsys


def lighten(c, amount=0.5):
    rgb = matplotlib.colors.to_rgb(c)
    h, l, s = colorsys.rgb_to_hls(*rgb)
    l = l * (1 - amount) + 1 * amount
    return colorsys.hls_to_rgb(h, l, s)
