#!/usr/bin/env python3

import warnings

import numpy
import pandas
import rpy2.robjects
import rpy2.robjects.packages
import rpy2.robjects.pandas2ri

import py_utils.rpy_helper


def glm_nb(data, formula):
    """
    Run negative binomial GLM
    :param data: pandas Dataframe with one column per feature and one row per sample.
    :param formula: string, representing formula in R-compatible syntax.
      Each element of the formula should represent column names in data.
    :return: pandas dataframe with overall stats and term stats.
    """
    base = rpy2.robjects.packages.importr('base')
    stats = rpy2.robjects.packages.importr('stats')
    mass = rpy2.robjects.packages.importr('MASS')
    car = rpy2.robjects.packages.importr('car')

    with rpy2.robjects.conversion.localconverter(rpy2.robjects.default_converter + rpy2.robjects.pandas2ri.converter):
        formula = rpy2.robjects.Formula(formula)
        for name, column in data.items():
            try:
               formula.environment[name] = column
            except ValueError as e:
                warnings.warn('Unable to convert {} to with rpy2: {}'.format(name, e))

    res = mass.glm_nb(formula)

    coef = py_utils.rpy_helper.r_matrix_to_df(stats.coef(base.summary(res)))

    confint = py_utils.rpy_helper.r_matrix_to_df(stats.confint(res))

    overall_stats = py_utils.rpy_helper.r_matrix_to_df(car.Anova(res, type='III'))
    overall_stats.index.names = ['variable']
    n = py_utils.rpy_helper.r_vector_to_series(stats.nobs(res))
    overall_stats = pandas.concat(
        [overall_stats,
         pandas.Series(numpy.repeat(n.values, overall_stats.shape[0]), name='n', index=overall_stats.index)], axis=1)
    overall_stats.rename(columns={'Pr(>Chisq)': 'per_term_pvalue'}, inplace=True)
    overall_stats.drop(columns=['LR Chisq', 'Df'], inplace=True)

    # Assumes that variable of interest is the first one in the formula. TODO: make it work regardless
    var_of_interest = overall_stats.index.get_level_values('variable').unique().to_list()[0]

    per_term_stats = pandas.concat([coef, confint], axis=1)
    per_term_stats.index.names = ['index']
    per_term_stats = per_term_stats.loc[
        per_term_stats.index.get_level_values('index').str.startswith(var_of_interest)]
    per_term_stats.rename(columns={'Estimate': 'estimate', '2.5 %': 'low_ci', '97.5 %': 'high_ci'}, inplace=True)
    per_term_stats = per_term_stats[['estimate', 'low_ci', 'high_ci']]
    per_term_stats['level'] = per_term_stats.index.get_level_values('index').str.replace(var_of_interest, '').to_list()

    # unlist(res$xlevels)[1]

    levels = []
    for _, row in per_term_stats.iterrows():
        levels.append(row.level)
    # ref_level = list(set(data[var_of_interest]).difference(set(levels)))
    per_term_stats['ref_level'] = list(set(data[var_of_interest]).difference(set(levels)))
    per_term_stats['variable'] = var_of_interest
    # per_term_stats['label'] = '{} (ref. {}'.format(*level, *ref_level)

    per_term_stats.set_index(['variable', 'ref_level', 'level'], drop=True, inplace=True)

    return overall_stats.join(per_term_stats)
