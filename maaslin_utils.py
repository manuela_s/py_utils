import pandas
import rpy2
import rpy2.robjects.packages

import rpy2.robjects
import rpy2.robjects.pandas2ri
import tempfile

import typing


class Maaslin:
    def __init__(
        self,
        input_data: pandas.DataFrame,
        input_metadata: pandas.DataFrame,
        normalization: typing.Literal["TSS", "CLR", "CSS", "NONE", "TMM"],
        transform: typing.Literal["LOG", "LOGIT", "AST", "NONE"],
        analysis_method: typing.Literal["LM", "CPLM", "NEGBIN", "ZINB"],
        random_effects: list[str],  # column names in `input_metadata`
        fixed_effects: list[str],  # column names in `input_metadata`
        reference: dict[str, str],  # the factors to use as a reference level for a categorical variable. Dictionary mapping column names to reference levels.
        standardize: bool = False,
        min_abundance: float = 0.0,
        min_prevalence: float = 0.1,
        min_variance: float = 0.0,
        max_significance: float = 0.25,
    ):
        maaslin2_r = rpy2.robjects.packages.importr("Maaslin2")
        outdir = tempfile.TemporaryDirectory()
        with (rpy2.robjects.default_converter + rpy2.robjects.pandas2ri.converter).context():
            self._fit_data = maaslin2_r.Maaslin2(
                input_data=input_data,
                input_metadata=input_metadata,
                output=outdir.name,
                normalization=normalization,
                transform=transform,
                analysis_method=analysis_method,
                random_effects=','.join(random_effects),
                fixed_effects=','.join(fixed_effects),
                reference=';'.join(f'{k},{v}' for (k, v) in reference.items()),
                standardize=standardize,
                min_abundance=min_abundance,
                min_prevalence=min_prevalence,
                min_variance=min_variance,
                max_significance=max_significance,
                correction="BH",
                plot_heatmap=False,
                plot_scatter=False,
            )

    @property
    def results(self):
        return self._fit_data['results']

    @property
    def residuals(self):
        return self._fit_data['residuals']

    @property
    def fitted(self):
        return self._fit_data['fitted']

    @property
    def ranef(self):
        return self._fit_data['ranef']
