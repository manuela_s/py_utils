from typing import Tuple, Union, Optional

import matplotlib.axes
import matplotlib.colors
import matplotlib.pyplot
import matplotlib.ticker


def range_legend(
        norm: matplotlib.colors.Normalize,
        sizes: Union[float, Tuple[float, float]] = 36,
        palette: Union[str, matplotlib.colors.Colormap] = 'k',
        ax: Optional[matplotlib.axes.Axes] = None,
        parent: Optional[Union[matplotlib.axes.Axes, matplotlib.figure.Figure]] = None,
        **kwargs):
    """
    Create legend entries for a range of values.
    :param norm: Normalization object to get value range from.
    :param sizes: Size, or tuple with [min, max] size to map values to.
    :param palette: color, or colormap to map values to.
    :param ax: axes where to create objects for legend. Defaults to gca.
    :param parent: Parent object to create legend in. Defaults to ax.
    :param kwargs: Keyword arguments for legend.
    :return: legend handle
    """
    if isinstance(norm, matplotlib.colors.LogNorm):
        locator = matplotlib.ticker.LogLocator(base=norm._scale.base)
        formatter = matplotlib.ticker.LogFormatterSciNotation(base=norm._scale.base)
    elif isinstance(norm, matplotlib.colors.SymLogNorm):
        locator = matplotlib.ticker.SymmetricalLogLocator(base=norm._scale.base, linthresh=norm._scale.linthresh)
        formatter = matplotlib.ticker.LogFormatterSciNotation(base=norm._scale.base)
    else:
        locator = matplotlib.ticker.AutoLocator()
        formatter = matplotlib.ticker.ScalarFormatter()

    if ax is None:
        ax = matplotlib.pyplot.gca()

    if parent is None:
        parent = ax

    values = locator.tick_values(norm.vmin, norm.vmax)
    values = [v for v in values if norm.vmin <= v <= norm.vmax]

    formatter.create_dummy_axis()
    formatter.set_locs(values)
    for value in values:
        if isinstance(sizes, tuple):
            s = norm(value) * (sizes[1]-sizes[0]) + sizes[0]
        else:
            s = sizes
        if isinstance(palette, matplotlib.colors.Colormap):
            color = palette(norm(value))
        else:
            color = palette
        label = formatter(value)
        ax.scatter([], [], s=s, color=color, label=label)
    return parent.legend(**kwargs)
