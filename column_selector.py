#!/usr/bin/env python3

class ColumnSelector:
    """Helper class to be used with ColumnTransformer for dataframes with multi-index columns.
    The class provides a callable that can be passed as column(s) definition of the transformer tuple."""
    def __init__(self, key, empty_ok=False):
        self.key = key
        self.empty_ok = empty_ok

    def __call__(self, X):
        if self.empty_ok and not (self.key in X.columns):
            return slice(0, 0, None)
        key = X.columns.get_loc(self.key)
        # Convert key slice to be slice of integers for compatibility with ColumnTransformer
        return slice(int(key.start), int(key.stop))
