#!/usr/bin/env python3

import pandas
import rpy2.robjects
import rpy2.robjects.pandas2ri


def r_vector_to_series(r_vector: rpy2.robjects.vectors.FloatVector) -> pandas.Series:
    """
    Helper function to convert r_vactor from rpy2 to pandas Series.
    :param r_vector: R vector object to convert to pandas Series;
    :return: converted pandas Series.
    """
    return pandas.Series(dict(r_vector.items()))


def r_matrix_to_df(r_matrix: rpy2.robjects.vectors.FloatMatrix) -> pandas.DataFrame:
    """
    Helper function to convert r_matrix from rpy2 to pandas DataFrame.
    :param r_matrix: R matrix object to convert to pandas DataFrame;
    :return: converted pandas DataFrame with index set to rownames and column names based on columns from r_matrix.
    """
    with rpy2.robjects.conversion.localconverter(
        rpy2.robjects.default_converter + rpy2.robjects.pandas2ri.converter
    ):
        return pandas.DataFrame(
            rpy2.robjects.conversion.rpy2py(r_matrix),
            index=r_matrix.rownames,
            columns=r_matrix.colnames,
        )
