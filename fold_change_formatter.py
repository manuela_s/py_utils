#!/usr/bin/env python3

import matplotlib.ticker


class FoldChangeFormatter(matplotlib.ticker.Formatter):
    def __call__(self, x, pos=None):
        if x.is_integer():
            return '{}X'.format(int(x))
        elif (1/x).is_integer():
            return r'$\frac{{1}}{{{}}}$ X'.format(int(1/x))
        else:
            return '{}X'.format(x)
