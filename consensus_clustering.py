#!/usr/bin/env python3

import collections.abc
import numbers

import joblib
import numpy
import scipy.cluster.hierarchy
import seaborn
import sklearn.base
import sklearn.cluster
import sklearn.ensemble
import sklearn.preprocessing
import sklearn.utils


class ConsensusClustering(sklearn.ensemble.BaseEnsemble, sklearn.base.ClusterMixin):
    """"""

    def __init__(self, n_clusters=2, estimator=None, n_estimators=100, frac_samples=0.8, random_state=None):

        self.n_clusters = n_clusters
        self.frac_samples = frac_samples
        self.random_state = random_state
        super(ConsensusClustering, self).__init__(
            estimator=estimator,
            n_estimators=n_estimators,
            estimator_params=('n_clusters',)
        )


    def fit(self, X):
        X = sklearn.utils.check_array(X)
        self._validate_estimator(sklearn.cluster.AgglomerativeClustering())

        random_state = sklearn.utils.check_random_state(self.random_state)
        self.n_samples_ = X.shape[0]
        # n_features_ = X.shape[1]
        connectivity_matrix = numpy.zeros((self.n_samples_, self.n_samples_))
        indicator_matrix = numpy.zeros(connectivity_matrix.shape)
        n_sub_samples = int(self.frac_samples * self.n_samples_)

        # Prepare estimators for each clustering iteration
        estimators = [self._make_estimator(append=False, random_state=random_state) for i in range(self.n_estimators)]
        # Pick what samples to cluster for each iteration:
        samples_per_estimator = [random_state.choice(range(self.n_samples_), n_sub_samples, replace=False)
                                 for i in range(self.n_estimators)]

        # Fit estimators in parallel
        estimators = joblib.Parallel()(
            joblib.delayed(e.fit)(X[samples_h,])
            for (e, samples_h) in zip(estimators, samples_per_estimator))

        # Aggregate results from estimators
        for (e, samples_h) in zip(estimators, samples_per_estimator):
            connectivity_matrix[numpy.ix_(samples_h, samples_h)] += (e.labels_.reshape(-1,1) == e.labels_.reshape((1,-1)))
            indicator_matrix[numpy.ix_(samples_h, samples_h)] += 1

        self.consensus_distance_ = 1 - (connectivity_matrix / indicator_matrix)
        self.Z_ = scipy.cluster.hierarchy.linkage(self.consensus_distance_[numpy.triu_indices(self.n_samples_, k=1)], 'ward')
        self.labels_ = scipy.cluster.hierarchy.fcluster(self.Z_, self.n_clusters, 'maxclust') -1
        self.sample_order_ = scipy.cluster.hierarchy.leaves_list(self.Z_)

        return self

    def plot_consensus_distance(self, ax):
        seaborn.heatmap(self.consensus_distance_[numpy.ix_(self.sample_order_, self.sample_order_)],
                        vmin=0,
                        vmax=1,
                        cmap='Blues_r',
                        ax=ax)

    def cdf(self, c):
        """Compute Cumulative Distribution Function for an array c"""
        # Make sure c is a one-dimensional numpy array:
        if isinstance(c, numbers.Number):
            c = numpy.array([c])
        elif isinstance(c, collections.abc.Iterable) and not isinstance(c, numpy.ndarray):
            c = numpy.array(c)
        assert(c.ndim == 1)

        consensus_indexes = 1-self.consensus_distance_[numpy.triu_indices(self.n_samples_, k=1)]
        return (consensus_indexes.reshape((-1,1)) <= c).mean(axis=0)

    def plot_consensus_index_histogram(self, ax):
        seaborn.distplot(1 - self.consensus_distance_.flatten(), hist_kws={'range': [0, 1]}, ax=ax)

    def plot_cdf(self, ax):
        x = numpy.arange(0, 1, 0.001)
        seaborn.lineplot(x, self.cdf(x), ax=ax)

    @property
    def auc(self):
        "Calculate Area Under the Curve for CDF curve"
        return self.cdf(numpy.arange(0, 1, 0.001)).mean()


def plot_delta_auc(cc_list, ax):
    "Plot delta auc curve for a list of consensus cluster objects"
    n_clusters = [cc.n_clusters for cc in cc_list]
    auc = numpy.array([cc.auc for cc in cc_list])
    auc[1:] = (auc[1:] - auc[:-1])/auc[:-1]
    ax.plot(n_clusters, auc, '-o')
    ax.set_xlabel('# clusters')
    ax.set_ylabel('Delta AUC')


def __test():
    import py_utils.consensus_clustering, matplotlib.pyplot, seaborn, numpy, pandas
    ax = matplotlib.pyplot.subplots(2,4)[1]
    test_ix = numpy.random.choice(range(300), 300, replace=False)
    test_X =numpy.concatenate([numpy.random.normal(size=[100, 10])-1, numpy.random.normal(size=[100, 10]), numpy.random.normal(size=[100, 10])+1])[test_ix]
    test_Y = numpy.array(['a']*100 + ['b']*100 + ['c']*100)[test_ix]
    seaborn.scatterplot(test_X[:,1], test_X[:,2], hue=test_Y, ax=ax[0,0])
    cc = py_utils.consensus_clustering.ConsensusClustering(n_clusters=3).fit(test_X)
    seaborn.heatmap(pandas.crosstab(test_Y, cc.labels_), cmap='Blues', ax=ax[0,1])
    cc.plot_consensus_distance(ax=ax[0,2])
    cc.plot_consensus_index_histogram(ax=ax[1,0])
    cc.plot_cdf(ax=ax[1,1])
    cc_list = []
    for k in range(2,11):
        cc = py_utils.consensus_clustering.ConsensusClustering(n_clusters=k).fit(test_X)
        cc.plot_cdf(ax=ax[1,2])
        cc_list.append(cc)
    py_utils.consensus_clustering.plot_delta_auc(cc_list, ax[1,3])
