#!/usr/bin/env python3

import sklearn.base


class FromPandasTransformer(sklearn.base.BaseEstimator, sklearn.base.TransformerMixin):
    """Transformer that transforms a pandas dataframe to a numpy array and exposes the column names as feature names."""

    def __init__(self, level=None):
        """
        Construct FromPandasTransformer
        :param level: If set, only include columns names from this level, assuming multi-index column names.
          Default is to include all names, joined by '_'.
        """
        super().__init__()
        self.level = level

    def fit(self, X, y=None):
        self.columns_ = X.columns.copy()
        return self

    def transform(self, X):
        return X.values

    def get_feature_names(self):
        if self.level is not None:
            return self.columns_.get_level_values(self.level)
        else:
            return self.columns_.map('_'.join)
