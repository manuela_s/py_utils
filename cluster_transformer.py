#!/usr/bin/env python3

import pandas
import sklearn.base


class ClusterTransformer(sklearn.base.BaseEstimator, sklearn.base.TransformerMixin):
    """Transformer that runs clustering on the data and returns the cluster labels as features."""
    def __init__(self, base_estimator, classifier=None):
        """"Constructor for ClusterTransformer.
        Input arguments:
        - base_estimator: clustering estimator used for clustering;
        - classifier: classifier estimator used to cluster new data in transform. It will be trained on the clusters
          assigned in fit or fit_transform."""

        assert(isinstance(base_estimator, sklearn.base.ClusterMixin))
        assert(classifier is None or isinstance(classifier, sklearn.base.ClassifierMixin))
        self.base_estimator = base_estimator
        self.classifier = classifier

    def transform(self, X):
        if not self.classifier:
            raise Exception('ClusterTransformer requires a classifier to transform new data')
        output = self.classifier.predict(X)
        return pandas.DataFrame(output, index=X.index, columns=self.get_feature_names())

    def fit_transform(self, X, y=None):
        output = self.base_estimator.fit_predict(X)
        if self.classifier is not None:
            self.classifier.fit(X, output)
        return pandas.DataFrame(output.reshape([-1, 1]), index=X.index, columns=self.get_feature_names())

    def get_feature_names(self):
        return ['cluster']
