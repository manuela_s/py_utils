#!/usr/bin/env python3

import lifelines
import lifelines.utils
import pandas


def survival_stats(tab: pandas.DataFrame, duration_col: str, event_col: str, name: str):
    """
    Compute median and 95% confidence intervals (CI) for survival.
    Based on https://github.com/CamDavidsonPilon/lifelines/issues/277.
    :param tab: dataframe with survival data;
    :param duration_col: name of column in 'tab' that contains the event/censoring time;
    :param event_col: name of the column in 'tab' that contains the event information (`True` if the subject had an
           event, `False` if no event was observed and the subject was censored);
    :param name: label for survival type to use as index in the result;
    :return: dataframe with one row with index 'survival_type' and columns 'n', 'median', 'lower_95ci' and 'upper_95ci'.
    """
    tab = tab[[duration_col, event_col]].dropna()
    kmf = lifelines.KaplanMeierFitter().fit(tab[duration_col], tab[event_col])

    stats = pandas.DataFrame(
        [
            name,
            tab.shape[0],
            kmf.median_survival_time_,
            lifelines.utils.median_survival_times(kmf.confidence_interval_)[
                "KM_estimate_lower_0.95"
            ].loc[0.5],
            lifelines.utils.median_survival_times(kmf.confidence_interval_)[
                "KM_estimate_upper_0.95"
            ].loc[0.5],
        ]
    ).T
    stats.columns = ["survival_type", "n", "median", "lower_95ci", "upper_95ci"]
    stats.set_index("survival_type", inplace=True)

    return stats
