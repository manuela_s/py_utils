#!/usr/bin/env python3

import matplotlib.pyplot
import seaborn
import typing
import pandas

import py_utils.rounding
import py_utils.outlier_scale


def _outlier_helper(data, ax):
    # Check for outliers (data-points more extreme than 1.5 times the inter-quartile range)
    q1, q3 = data.expression.quantile([0.25, 0.75])
    upper = 1.5 * (q3 - q1) + q3
    highest_non_outlier = data.expression[data.expression < upper].max()
    threshold = py_utils.rounding.round_up_to_n_significant_digits(highest_non_outlier, 2)
    if data.expression.max() > threshold * 1.5:
        # Use logarithmic scale for outliers
        ax.axhline(threshold, color='k', linestyle=':')
        ax.set_yscale('symlog', linthresh=threshold, linscale=3)
        ax.yaxis.set_major_locator(py_utils.outlier_scale.OutlierLogLocator(linthresh=threshold, base=10))
        ax.yaxis.set_minor_locator(
            py_utils.outlier_scale.OutlierLogLocator(linthresh=threshold, base=10, subs=range(1, 9)))
        ax.yaxis.set_major_formatter(matplotlib.ticker.StrMethodFormatter('{x:g}'))

    return ax


def _get_params(data, grouping_variable, grouping_variable_order, palette):
    params = dict(x=grouping_variable,
                  y='expression',
                  order=grouping_variable_order,
                  hue=grouping_variable,
                  hue_order=grouping_variable_order,
                  palette=palette,
                  data=data)

    return params


def _highlight_summary_stats(s):
    """Modify quartile line objects to be more visible"""
    for l in s.findobj(matplotlib.lines.Line2D):
        if l.axes is None:
            continue
        l.set_zorder(3)
        l.set_linewidth(2)


def plot_boxplot(data: pandas.DataFrame, grouping_variable: str, grouping_variable_order: typing.Sequence,
                 palette='Reds', ax=None):
    """ Make a boxplot with overlaid swarmplot.
    :param data: data to plot, one row per observation and a column 'expression' with the expression values;
                 Indices should include grouping_variable with values from grouping_variable_order;
    :param grouping_variable: variable to group by;
    :param grouping_variable_order: order to plot grouping variable;
    :param palette: palette.
    """
    if ax is None:
        ax = matplotlib.pyplot.gca()

    ax = _outlier_helper(data, ax)

    # Make boxplot with overlaying swarmplot color-coded by grouping_variable
    params = _get_params(data, grouping_variable, grouping_variable_order, palette)

    b = seaborn.boxplot(dodge=False,
                        width=0.9,
                        saturation=1,
                        showfliers=False,
                        **params)

    s = seaborn.swarmplot(size=2,
                          **params)

    # Set transparency on boxplot fill color
    for patch in b.artists:
        r, g, b, a = patch.get_facecolor()
        patch.set_facecolor((r, g, b, 0.3))

    _highlight_summary_stats(s)


def plot_violinplot(data: pandas.DataFrame, grouping_variable: str, grouping_variable_order: typing.Sequence,
                    palette='Reds', ax=None):
    """ Make a violin plot with overlaid swarmplot.
    :param data: data to plot, one row per observation and a column 'expression' with the expression values;
                 Indices should include grouping_variable with values from grouping_variable_order;
    :param grouping_variable: variable to group by;
    :param grouping_variable_order: order to plot grouping variable;
    :param palette: palette.
    """
    if ax is None:
        ax = matplotlib.pyplot.gca()

    ax = _outlier_helper(data, ax)

    # Make violinplot with overlaying swarmplot color-coded by grouping_variable
    params = _get_params(data, grouping_variable, grouping_variable_order, palette)

    v = seaborn.violinplot(
        dodge=False,
        width=0.9,
        saturation=1,
        inner='quartile',
        cut=0,
        **params,
    )

    s = seaborn.swarmplot(
        size=2,
        **params,
    )

    # Set transparency on violin fill color
    for patch in ax.findobj(matplotlib.collections.PolyCollection):
        facecolor = patch.get_facecolor()
        facecolor[:, -1] = 0.3
        patch.set_facecolor(facecolor)

    _highlight_summary_stats(s)
