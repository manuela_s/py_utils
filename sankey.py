#!/usr/bin/env python3

import itertools
import json
import subprocess
import tempfile
import svglib.svglib
import reportlab.graphics.renderPDF


# From https://docs.python.org/3/library/itertools.html#itertools-recipes
def pairwise(iterable):
    """s -> (s0,s1), (s1,s2), (s2, s3), ..."""
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)


class PathSankey:
    def __init__(self, data):
        """Make SanKey diagram showing the path samples take through a set of layers.

        Uses https://ricklupton.github.io/d3-sankey-diagram/ and https://github.com/ricklupton/svg-sankey for actual
        plotting.

        :param data: pandas dataframe with the input data. One row per sample and one column per layer. Every unique
        value in each layer indicate what group the sample was part if in that layer.
        """
        if data.columns.name is None:
            data.columns.name = 'variables'

        self.data = data

    def create_node(self, layer, position):
        """
        Create a sankey node represeting one position in one layer.
        :param layer: Name of the layer
        :param position: Name of the position for the node
        :return: Dictionary with node definition.
        """
        return {
            'id': '{}_{}'.format(layer, position),
            'title': '{}'.format(position),
        }

    def create_link(self, source_layer, target_layer, source_position, target_position, weight, path_id):
        """Create a single sankey link, connecting two positions in neighboring layers
        :param source_layer:
        :param target_layer:
        :param source_position:
        :param target_position:
        :param weight:
        :param path_id:
        :return:
        """
        return {
            'source': '{}_{}'.format(source_layer, source_position),
            'target': '{}_{}'.format(target_layer, target_position),
            'value': weight,
            'type': path_id,
        }
        #   'color': get_color(index)

    def create_links_for_path(self, path, weight):
        """Create the links for one path through the layers.
        :param path: The path through the layers. Iterable with one element (name of position) for each layer (column in
        self.data)
        :param weight: The weight of the path (number of samples taking this path).
        :return: List of dictionaries with link definitions.
        """
        path_id = '_'.join(str(p) for p in path)
        return [
            self.create_link(*layers, *positions, weight, path_id)
            for (layers, positions) in zip(pairwise(self.data.columns), pairwise(path))

        ]

    def create_group(self, layer, series):
        """Generate a group for one layer of the plot
        :param layer:
        :param series:
        :return: Dictionary with group definition
        """
        return {
            'id': '{}'.format(layer),
            'title': '{}'.format(layer),
            'nodes': ['{}_{}'.format(layer, position) for position in series.unique()],
        }

    def get_order(self, layer, series):
        """Generate order for one layer of the plot
        :param layer: Name of layer (self.data column name)
        :param series: Series with data for layer
        :return: List with order definition
        """
        return ['{}_{}'.format(layer, position) for position in sorted(series.unique())]

    def get_sankey_data(self):
        """Generate the sankey datastructure used by d3-sankey-diagram.
        :return: dict with sankey data.
        """
        nodes = self.data.stack().to_frame('cluster').groupby([self.data.columns.name, 'cluster'], observed=True).size()
        paths = self.data.groupby(self.data.columns.to_list(), observed=True).size()
        return {
            'nodes': [self.create_node(layer, position) for ((layer, position), weight) in nodes.items()],
            'links': sum((self.create_links_for_path(path, weight) for (path, weight) in paths.items()), []),
            'groups': [self.create_group(layer, series) for (layer, series) in self.data.items()],
            'order': [[self.get_order(layer, series)] for (layer, series) in self.data.items()],
            'alignLinkTypes': True,
        }

    def generate_sankey(self, filename):
        with tempfile.NamedTemporaryFile(suffix='.json', mode='w') as tmp:
            json.dump(self.get_sankey_data(), tmp, indent=2)
            tmp.flush()
            sp = subprocess.run(['./node_modules/.bin/svg-sankey', tmp.name], capture_output=True)
        with open(tempfile.NamedTemporaryFile(suffix='.svg').name, 'wb') as f:
            # Workaround for https://github.com/ricklupton/svg-sankey/issues/1
            f.write(sp.stdout[sp.stdout.find(b'<?xml'):])
            reportlab.graphics.renderPDF.drawToFile(svglib.svglib.svg2rlg(f.name), filename + '.pdf')

    def to_pdf(self, filename):
        self.generate_sankey(filename)
