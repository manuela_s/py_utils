#!/usr/bin/env python3

import math


def round_up_to_n_significant_digits(number: float, significant_digits: int) -> float:
    """Round a number up to n significant digits.
    :param number: number to round;
    :param significant_digits: number of significant digits to round to;
    :return: rounded number.
    """
    digits = significant_digits - math.ceil(math.log10(abs(number)))
    return math.ceil(number * 10 ** digits) / 10 ** digits
