#!/usr/bin/env python3

import adjustText
import matplotlib.pyplot
import pandas
import seaborn
import matplotlib.pyplot
import py_utils.fold_change_formatter
import enum


class ScaleType(enum.Enum):
    FOLD_CHANGE = enum.auto()
    LINEAR = enum.auto()


def plot(data, contrast_1, contrast_2, xlim, ylim, scale_type=ScaleType.FOLD_CHANGE, style_col=None, style_markers=True):
    """
    Make volcano plot
    :param data: pandas Dataframe with one row per item and columns effect_size and adjusted_pvalue.
    :param contrast_1: string, name of reference group
    :param contrast_2: string, name of group being compared to reference group
    :param xlim: tuple with 2 elements, lower and upper limit effect_size
    :param ylim: tuple with 2 elements, lower and upper limit for adjusted pvalue
    :param scale_type: ScaleType enum. If FOLD_CHANGE, then effect_size column in data should represent fold_change,
    where 1 means no change. If LINEAR, then effect_size column in data should represent difference where 0 means no
    difference.
    :param style_col: string, name of column to use for styling. Defaults to not use styles.
    :param style_markers: Object determining how to draw the markers for different levels of the style variable.
    See seaborn.scatterplot.
    :return: matplotlib axes
    """

    if scale_type == ScaleType.FOLD_CHANGE:
        neutral_effect_size = 1
        xlabel = 'Fold change'
    elif scale_type == ScaleType.LINEAR:
        neutral_effect_size = 0
        xlabel = 'Differential change'

    data['significance'] = data.apply(
        lambda x: 'non-significant' if x.adjusted_pvalue > 0.05 else
        ('significant-increase' if x.effect_size > neutral_effect_size else 'significant-decrease'), axis=1)
    data['significance'] = pandas.Categorical(data['significance'],
                                              ['non-significant', 'significant-increase', 'significant-decrease'])

    ax = seaborn.scatterplot(
        x='effect_size',
        y='adjusted_pvalue',
        hue='significance',
        style=style_col,
        markers=style_markers,
        data=data,
        alpha=0.5,
        edgecolor='k',
        palette={'non-significant': 'gray', 'significant-increase': 'blue', 'significant-decrease': 'red'})

    ax.axvline(neutral_effect_size, color='k', linestyle=':')
    ax.axhline(0.05, color='k', linestyle=':')

    if scale_type == ScaleType.FOLD_CHANGE:
        ax.set_xscale('log', basex=2)
        ax.xaxis.set_major_formatter(py_utils.fold_change_formatter.FoldChangeFormatter())

    ax.set_xlim(xlim)
    ax.set_xlabel('{}, {} vs {}'.format(xlabel, contrast_1, contrast_2))

    ax.set_yscale('log')
    ax.set_ylim(ylim)
    ax.invert_yaxis()
    ax.set_ylabel('Adjusted p-value')

    matplotlib.pyplot.annotate(' P=0.05',
                               (xlim[0], 0.05),
                               verticalalignment='bottom')
    matplotlib.pyplot.annotate(' Higher in {} '.format(contrast_1),
                               (neutral_effect_size, ylim[0]),
                               verticalalignment='top',
                               horizontalalignment='left')
    matplotlib.pyplot.annotate(' Higher in {} '.format(contrast_2),
                               (neutral_effect_size, ylim[0]),
                               verticalalignment='top',
                               horizontalalignment='right')
    annotations = [matplotlib.pyplot.annotate(row.Index, (row.effect_size, row.adjusted_pvalue),
                                              fontsize='xx-small',
                                              arrowprops={'arrowstyle': '->'})
                   for row in data[data.adjusted_pvalue < 0.05].itertuples()]
    adjustText.adjust_text(annotations, data.effect_size.values, data.adjusted_pvalue.values, precision=0.0001)

    return ax
